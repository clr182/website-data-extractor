import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;

/*
Retrieve json data from an API giving the location of the ISS.
Parse JSON using org.json. The format of the expected response from the API is

{
  "message": "success",
  "timestamp": UNIX_TIME_STAMP,
  "iss_position": {
    "latitude": CURRENT_LATITUDE,
    "longitude": CURRENT_LONGITUDE
  }
}

 */
public class JSONParseSpaceStationLocation {
    public static void main(String[] args){
        final String ISS_NOW_URI = "http://api.open-notify.org/iss-now.json";

        extractStringFromWebPage(ISS_NOW_URI);
    }

    public static void extractStringFromWebPage(String webPage){
        try{

            URL url = new URL(webPage);
            URLConnection urlConnection = url.openConnection();
            InputStream websiteInput = urlConnection.getInputStream();
            InputStreamReader inputReader = new InputStreamReader(websiteInput);

            int numCharRead;
            char[] charArray = new char[1024];
            StringBuffer sb = new StringBuffer();
            while((numCharRead = inputReader.read(charArray)) >0 ) {
                sb.append(charArray, 0, numCharRead);
        }
            String allDataOnSite = sb.toString();

            System.out.println(allDataOnSite);


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
