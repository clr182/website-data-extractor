import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonTesting {
    public static void main(String[] args) {
        createJSONObject();
        createJSONFromMap();
        createJSONObjectFromJSONString();
        User user = new User("Jason", 26, "dundalk");

        serializeJavaObjectToJSON(user);
        createJSONArray();
        createJSONObjectFromJSONString();
        createJSONArrayFromJavaCollection();
        createJSONArrayFromCSV();
        createCSVFromJSONArray();
        createJSONArrayOfJSONObjectsFromCSV();
        createJSONArrayOfJSONObjectsFromCSVWithoutHeader();

    }
        public static void createJSONObject(){

        JSONObject jo = new JSONObject();
        jo.put("name", "jack clegg");
        jo.put("age", "48");
        jo.put("town", "Dundalk");

        System.out.println(jo);
    }

    public static void createJSONFromMap() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "Jason");
        map.put("age", "22");
        map.put("town", "Dundalk");

        JSONObject jo = new JSONObject(map);
        System.out.println(jo);
    }

    public static void createJSONObjectFromJSONString() {
        JSONObject jo = new JSONObject("{\"name\":\"Jason\",\"age\" : \"22\", \"town\":\"Dundalk\"}");
    }

    public static void serializeJavaObjectToJSON(User user) {
        JSONObject jo = new JSONObject(user);
        System.out.println(jo);
    }

    public static void createJSONArray(){
            JSONArray ja = new JSONArray();
            ja.put(Boolean.TRUE);

            JSONObject jo = new JSONObject();
            jo.put("name", "jack clegg");
            jo.put("age", "48");
            jo.put("town", "Dundalk");

            ja.put(jo);
            System.out.println(ja);
    }

    public static void createJSONArrayFromJSONString(){
        JSONArray ja = new JSONArray("[true, \"Jason\", 22]");
        System.out.println(ja);
    }

    public static void createJSONArrayFromJavaCollection(){
        List<String> list = new ArrayList<String>();
        list.add("Cameron");
        list.add("Thomas");
        list.add("Adam");

        JSONArray ja = new JSONArray(list);
        System.out.println(ja);
    }

    public static void createJSONArrayFromCSV(){
        JSONArray ja = CDL.rowToJSONArray(new JSONTokener("Nathan, Kristine, Neil"));
        System.out.println(ja);
    }

    public static void createCSVFromJSONArray(){
        JSONArray ja = new JSONArray(("[\"Pauric\", \"Nicholas\", \"Zehao\"]"));
        String csv = CDL.rowToString(ja);
        System.out.println(csv);
    }

    public static void createJSONArrayOfJSONObjectsFromCSV(){
        String string =
                "name, age, town \n" +
                "jason, 22, Dundalk \n" +
                "cameron, 22, Rostrevor \n" +
                "james, 18, Ardee hai";

        JSONArray result = CDL.toJSONArray(string);
        System.out.println(result);
    }

    public static void createJSONArrayOfJSONObjectsFromCSVWithoutHeader(){
        JSONArray ja = new JSONArray();
        ja.put("name");
        ja.put("age");
        ja.put("town");

        String string =
                        "jason, 22, Dundalk \n" +
                        "cameron, 22, Rostrevor \n" +
                        "james, 18, Ardee hai";

        JSONArray result = CDL.toJSONArray(ja, string);
        System.out.println(result);
    }
}


